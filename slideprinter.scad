module line_from_to(v0, v1, r = 1.0){
  v2 = v1 - v0;
  color("yellow")
    if(len(v2) == 3){
      v2l = sqrt(v2[0]*v2[0] + v2[1]*v2[1] + v2[2]*v2[2]);
      v2n = v2/v2l;
      theta = acos(v2n[2]);
      phi   = acos(v2n[1]/sqrt(v2n[1]*v2n[1] + v2n[0]*v2n[0]));
      //echo(theta);
      //echo(phi);
      translate(v0)
        if(v2n[0] < 0){
          rotate([-theta,0,phi])
            cylinder(r=r, h=v2l);
        } else {
          rotate([-theta,0,-phi])
            cylinder(r=r, h=v2l);
        }
    } else {
      v2l = sqrt(v2[0]*v2[0] + v2[1]*v2[1]);
      v2n = v2/v2l;
      phi   = acos(v2n[1]/sqrt(v2n[1]*v2n[1] + v2n[0]*v2n[0]));
      translate(v0)
        if(v2n[0] < 0){
          rotate([-90,0,phi])
            cylinder(r=r, h=v2l);
        } else {
          rotate([-90,0,-phi])
            cylinder(r=r, h=v2l);
        }
    }
}






ANCHOR_A = [0,-100,0];
ANCHOR_B = [cos(30)*100,sin(30)*100,0];
ANCHOR_C = [-cos(30)*100,sin(30)*100,0];




for(k=[0:120:359])
rotate([0,0,k])
rotate([90,0,0])
translate([0,0,100])
sphere(d=3);

color("yellow")
translate(ANCHOR_A)
sphere(d=4);
color("yellow")
translate(ANCHOR_B)
sphere(d=4);
color("yellow")
translate(ANCHOR_C)
sphere(d=4);

// Simulating G1 comands
Pos = [20*cos($t*360),20*sin($t*360),0];

// Pythagoras' theorem calculating line lengths
l_A = sqrt((Pos[0] - ANCHOR_A[0])*(Pos[0] - ANCHOR_A[0]) + (Pos[1] - ANCHOR_A[1])*(Pos[1] - ANCHOR_A[1]));
l_B = sqrt((Pos[0] - ANCHOR_B[0])*(Pos[0] - ANCHOR_B[0]) + (Pos[1] - ANCHOR_B[1])*(Pos[1] - ANCHOR_B[1]));
l_C = sqrt((Pos[0] - ANCHOR_C[0])*(Pos[0] - ANCHOR_C[0]) + (Pos[1] - ANCHOR_C[1])*(Pos[1] - ANCHOR_C[1]));
echo(l_C);

// Print head
translate(Pos)
cylinder(d=5, h=10);

color("red"){
line_from_to(ANCHOR_A, Pos, r=0.5);
//line_from_to(ANCHOR_B, Pos, r=0.5);
//line_from_to(ANCHOR_C, Pos, r=0.5);
}

// The other Slideprinter...
OFFSET = [10, 1, 0];
ANCHOR_A_sl = ANCHOR_A + OFFSET;
ANCHOR_B_sl = ANCHOR_B + OFFSET;
ANCHOR_C_sl = ANCHOR_C + OFFSET;

Pos_sl = Pos + OFFSET;

color("lightgreen"){
translate(ANCHOR_A_sl)
sphere(d=4);
translate(ANCHOR_B_sl)
sphere(d=4);
translate(ANCHOR_C_sl)
sphere(d=4);
}

translate(Pos_sl)
cylinder(d=5, h=10);

color("pink"){
//line_from_to(ANCHOR_A_sl, Pos_sl, r=0.5);
line_from_to(ANCHOR_B_sl, Pos_sl, r=0.5);
//line_from_to(ANCHOR_C_sl, Pos_sl, r=0.5);
}


// The third Slideprinter...
OFFSET_C = [1, 10, 0];
ANCHOR_A_slC = ANCHOR_A + OFFSET_C;
ANCHOR_B_slC = ANCHOR_B + OFFSET_C;
ANCHOR_C_slC = ANCHOR_C + OFFSET_C;

Pos_slC = Pos + OFFSET_C;

color("lightgreen"){
translate(ANCHOR_A_slC)
sphere(d=4);
translate(ANCHOR_B_slC)
sphere(d=4);
translate(ANCHOR_C_slC)
sphere(d=4);
}

translate(Pos_slC)
cylinder(d=5, h=10);

color([0.1,0.7,0.7]){
//line_from_to(ANCHOR_A_slC, Pos_slC, r=0.5);
//line_from_to(ANCHOR_B_slC, Pos_slC, r=0.5);
line_from_to(ANCHOR_C_slC, Pos_slC, r=0.5);
}

color("white")
translate(Pos_slC-[-2.5, 6, 0])
cylinder(d1=0, d2=3, h=10);
